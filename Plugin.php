<?php namespace Greymen\Subscribers;

use System\Classes\PluginBase;
use Greymen\Subscribers\Classes\ActiveCampaign;
use Greymen\Subscribers\Models\Campaigns;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Greymen Subscribers',
            'description' => 'Subscribe email addresses and passes them on to Active Campaign or MailChimp.',
            'author'      => 'Greymen',
            'icon'        => 'icon-envelope-square',
            'homepage'    => 'https://greymen.co/'
        ];
    }

    public function boot()
    {
        \RainLab\Pages\Classes\Page::extend(function($model) {
            $model->addDynamicMethod('getPageOptions', function() {
                $theme = \Cms\Classes\Theme::getEditTheme();
                $pageList = new \RainLab\Pages\Classes\PageList($theme);
                $pages = [];
                foreach ($pageList->listPages() as $name => $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . ' (' . $pageObject->url . ')';
                }
                return $pages;
            });
            $model->addDynamicMethod('getListOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getLists();
            });
            $model->addDynamicMethod('getAcFieldNameOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getFields();
            });
            $model->addDynamicMethod('getFormIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });
            $model->addDynamicMethod('getCampaignIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });

        });
    }
    public function registerComponents()
    {
        return
        [
            'Greymen\Subscribers\Components\Contact'       => 'Contact',
            'Greymen\Subscribers\Components\Newsletter'    => 'Newsletter',
            'Greymen\Subscribers\Components\Form'          => 'Form',
        ];
    }
    public function registerMailTemplates()
    {
        return [
            'greymen.subscribers::mail.subscribed',
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'greymen.subscribers::lang.plugin.name',
                'category' => 'system::lang.system.categories.cms',
                'icon' => 'oc-icon-user',
                'description' => 'greymen.subscribers::lang.plugin.description',
                'class' => 'Greymen\Subscribers\Models\Settings',
                'order' => 800,
            ]
        ];
    }
    public function registerReportWidgets()
    {
        return [
            'Greymen\Subscribers\ReportWidgets\SubscriberStats' => [
                'label'   => 'Subscribers stats',
                'context' => 'dashboard'
            ]
        ];
    }


}
