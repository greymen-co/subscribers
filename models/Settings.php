<?php
namespace Greymen\Subscribers\Models;

use Model;

/**
 * Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'greymen_subscribers_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
